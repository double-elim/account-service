## The Account Service

This bracket management software consists of 2 main parts: accounts and series. Accounts can be either head TOs or volunteer TOs. Series (think Genesis) contain tournaments (Genesis 1, 2, 3, etc.), and tournaments contain events (singles, doubles).

The purpose of the Account Service is to handle the creation of Head TOs and Volunteer TOs.
